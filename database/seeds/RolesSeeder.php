<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $super_admin = Role::create(['name'=>'super-admin']);
        $administrador = Role::create(['name'=>'administrador']);
        $caja = Role::create(['name'=>'caja']);
        $cocina = Role::create(['name'=>'cocina']);
        $mesero = Role::create(['name'=>'mesero']);
        $repartidor = Role::create(['name'=>'repartidor']);


        //
    }
}

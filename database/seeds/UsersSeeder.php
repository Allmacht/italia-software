<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'              => 'SAdmin',
            'token'             => '1234',
            'names'             => 'SAdmin',
            'paternal_surname'  => 'SAdmin',
            'maternal_surname'  => 'SAdmin',
            'birthdate'         => '1990-12-31',
            'nationality'       => 'Mexicana',
            'marital_status'    => 'soltero',
            'street'            => 'example street',
            'neighborhood'      => 'example neighborhood',
            'external_number'   => '555',
            'internal_number'   => 'A',
            'gender'            => 'Masculino',
            'zipcode'           => '55555',
            'phone'             => '4444444444',
            'email'             => 'ulises.jacob.cr@gmail.com',
            'medical_insurance' => '12345678901',
            'emergency_contact' => 'emergency contact',
            'contact_number'    => '4444444444',
            'medication'        => 'medication',
            'rfc'               => '12345678901',
            'authorized'        => true,
            'admission_date'    => '2019-01-01',
            'password'          => bcrypt('admin'),
            'status'            => true
        ]);

        $user = User::first();
        $user->assignRole('super-admin');
    }
}

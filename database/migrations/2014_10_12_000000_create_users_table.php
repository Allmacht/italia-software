<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('token');
            $table->string('name');
            $table->string('names');
            $table->string('paternal_surname');
            $table->string('maternal_surname');
            $table->date('birthdate');
            $table->string('nationality');
            $table->string('marital_status');
            $table->string('street');
            $table->string('neighborhood')->nullable();
            $table->integer('external_number');
            $table->set('gender',['Masculino','Femenino']);
            $table->string('internal_number')->nullable();
            $table->integer('zipcode');
            $table->string('rfc')->nullable();
            $table->string('phone');
            $table->string('email')->unique();
            $table->string('medical_insurance')->unique();
            $table->string('emergency_contact');
            $table->string('contact_number');
            $table->string('medication')->nullable();
            $table->date('admission_date');
            $table->string('image')->nullable();
            $table->float('salary')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('status')->default(true);
            $table->boolean('authorized')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

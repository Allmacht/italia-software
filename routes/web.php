<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->middleware(['CheckStatus','auth','CheckEntry'])->name('home');
Route::post('/users/store','UserController@store')->name('users.store');

Route::middleware(['auth','role:super-admin|administrador','CheckStatus','CheckEntry'])->prefix('users')->name('users.')->group(function(){
    Route::get('/', 'UserController@index')->name('index');
    Route::get('/user/{id}','UserController@show')->name('show')->where('id','[0-9]+');
    Route::get('/user/{id}/edit','UserController@edit')->name('edit')->where('id','[0-9]+');
    Route::get('/user/{id}/roles','UserController@roles')->name('roles');

    Route::post('/disable','UserController@disable')->name('disable');
    Route::post('/enable','UserController@enable')->name('enable');
    Route::post('/destroy','UserController@destroy')->name('destroy');
    Route::post('/authorize','UserController@authorizeAccount')->name('authorize');
    Route::post('/update/{id}','UserController@update')->where('id','[0-9]+')->name('update');
    Route::post('/roles/{id}/update','UserController@update_roles')->where('id','[0-9]+')->name('roles.update');
});

Route::middleware(['auth','role:super-admin|administrador','CheckStatus','CheckEntry'])->prefix('attendances')->name('attendances.')->group(function(){
    Route::get('/','AttendanceController@index')->name('index');
    Route::any('/registers','AttendanceController@registers')->name('registers');

    Route::post('/store','AttendanceController@store')->name('store');
    Route::post('departiture','AttendanceController@departiture')->name('departiture');
});

Route::middleware(['auth','role:super-admin|administrador|cocina','CheckStatus','CheckEntry'])->prefix('stews')->name('stews.')->group(function(){
    Route::get('/','StewController@index')->name('index');

    Route::post('/store','StewController@store')->name('store');
    Route::post('/disable/{id}','StewController@disable')->name('disable')->where('id','[0-9]+');
    Route::post('/enable/{id}','StewController@enable')->name('enable')->where('id','[0-9]+');
});

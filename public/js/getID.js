$(document).ready(function(){
    $('.open-modal').click(function(){

        if($(this).data('action') == 'disable'){
            $('#disable-id').val($(this).data('id'));
        }
        if($(this).data('action') == 'delete'){
            $('#delete-id').val($(this).data('id'));
        }
        if($(this).data('action') == 'enable'){
            $('#enable-id').val($(this).data('id'));
        }
        if($(this).data('action') == 'authorize'){
            $('#authorize-id').val($(this).data('id'));
        }
        if($(this).data('action') == 'departiture'){
            $('#attendance-id').val($(this).data('id'));
        }

    });
});

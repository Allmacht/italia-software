@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/auth/login.css')}}">
@endsection
@section('content')

    <div class="container mt-4">
        <div class="row">
            <div class="col-12 form-row px-0 div-1 shadow">

                <div class="col-lg-8 div-2 px-0">

                    <div class="col-xl-12 text-center py-5">
                        <img src="{{asset('images/logo-white.jpg')}}" class="img-fluid" alt="" width="350px">
                    </div>

                    <form class="col-12" action="" method="post">
                        @csrf
                        <div class="form-group col-xl-8 mx-auto mb-5">
                            <input type="text" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{old('email')}}" placeholder="Correo electrónico" required>
                        </div>

                        <div class="form-group col-xl-8 mx-auto mb-5">
                            <input type="password" class="form-control form-control-lg @error('email') is-invalid @enderror" name="password" placeholder="Contraseña" required>
                        </div>

                        <div class="col-xl-8 mx-auto mb-5 text-center">
                            <a href="#" class="btn btn-link text-decoration-none mb-5">
                                {{__('¿Ha olvidado su contraseña?')}}
                            </a>

                            <button type="submit" class="btn btn-block btn-login rounded-pill py-2">
                                {{__('INICIAR SESIÓN')}}
                            </button>
                        </div>

                    </form>

                </div>
                <div class="col-lg-4 div-3 px-0">

                    <div class="col-12 text-center py-5">
                        <h3>{{__('¿Nuevo aquí?')}}</h3>
                        <p class="mt-5">{{__('Al registrarte, tu cuenta necesitará ser activada por un Administrador')}}</p>

                        <a href="{{route('register')}}" class="btn rounded-pill btn-register">
                            {{__('REGISTRARSE')}}
                        </a>
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection

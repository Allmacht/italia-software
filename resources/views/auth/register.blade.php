@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/auth/login.css')}}">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 div-1 mt-4 py-4 shadow">
                <div class="col-12 text-center">
                    <img src="{{asset('images/logo-white.jpg')}}" alt="" class="img-fluid" width="300px">
                </div>
                <div class="col-12 text-center alert alert-warning shadow">
                    <p class="mb-0">{{__('Ingrese toda la información requerida. Al finalizar su registro, su cuenta necesitará ser validada por un administrador.')}}</p>
                </div>

                <form class="col-12" action="{{route('users.store')}}" method="post">
                    @csrf
                    <div class="col-12 form-row mt-4">
                        <div class="col-12 mt-2 mb-3">
                            <h5 class="text-muted"><i class="fad fa-user mr-2"></i> {{__('Información personal')}}</h5>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('names') is-invalid @enderror" name="names" value="{{old('names')}}" placeholder="Nombre(s)" required>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('paternal_surname') is-invalid @enderror" name="paternal_surname" value="{{old('paternal_surname')}}" placeholder="Apellido paterno" required>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('maternal_surname') is-invalid @enderror" name="maternal_surname" value="{{old('maternal_surname')}}" placeholder="Apellido materno" required>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('birthdate') is-invalid @enderror" name="birthdate" value="{{old('birthdate')}}" placeholder="Fecha de nacimiento" onfocus="(this.type='date')" onblur="(this.type='text')" value="{{old('birthdate')}}" required>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('nationality') is-invalid @enderror" name="nationality" value="{{old('nationality')}}" placeholder="Nacionalidad" required>
                        </div>
                        <div class="form-group col-lg-2 col-md-6 mb-4">
                            <select class="form-control" name="marital_status" required>
                                <option value="" disabled selected>{{__('Estado civil')}}</option>
                                <option value="Soltero/a">{{__('Soltero/a')}}</option>
                                <option value="Casado/a">{{__('Casado/a')}}</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-2 col-md-6 mb-4">
                            <select class="form-control" name="gender" required>
                                <option value="" disabled selected>{{__('Sexo')}}</option>
                                <option value="Masculino">{{__('Masculino')}}</option>
                                <option value="Femenino">{{__('Femenino')}}</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{old('phone')}}" placeholder="Teléfono" required min="1" max="9999999999">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('medical_insurance') is-invalid @enderror" name="medical_insurance" value="{{old('medical_insurance')}}" placeholder="N° seguro social" required>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('emergency_contact') is-invalid @enderror" name="emergency_contact" value="{{old('emergency_contact')}}" placeholder="Contacto de emergencía" required>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="number" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" value="{{old('contact_number')}}" placeholder="Teléfono de emergencía" required min="1" max="9999999999">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('medication') is-invalid @enderror" name="medication" value="{{old('medication')}}" placeholder="Medicamento controlado">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('rfc') is-invalid @enderror" name="rfc" value="{{old('rfc')}}" placeholder="R.F.C.">
                        </div>
                    </div>
                    <div class="col-12 form-row mt-4">
                        <div class="col-12 mt-2 mb-3">
                            <h5 class="text-muted"><i class="fad fa-map-marker-alt mr-2"></i>{{__('Domicilio')}}</h5>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('street') is-invalid @enderror" name="street" value="{{old('street')}}" placeholder="Calle" required>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('neighborhood') is-invalid @enderror" name="neighborhood" value="{{old('neighborhood')}}" placeholder="Colonia/Fraccionamiento">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="number" class="form-control @error('external_number') is-invalid @enderror" name="external_number" value="{{old('external_number')}}" placeholder="Número exterior" required min="1">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" class="form-control @error('internal_number') is-invalid @enderror" name="internal_number" value="{{old('internal_number')}}" placeholder="Número interior">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="number" class="form-control @error('zipcode') is-invalid @enderror" name="zipcode" value="{{old('zipcode')}}" placeholder="Código postal" required min="1" max="99999">
                        </div>
                    </div>
                    <div class="col-12 form-row mt-4">
                        <div class="col-12 mt-2 mb-3">
                            <h5 class="text-muted"><i class="fad fa-user-lock mr-2"></i>{{__('Usuario')}}</h5>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 mb-4">
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nombre de usuario" value="{{old('name')}}" required>
                        </div>
                        <div class="form-group col-lg-8 col-md-6 mb-4">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Correo electrónico" value="{{old('email')}}" required>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 mb-4">
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 mb-4">
                            <input type="password" class="form-control @error('password-confirmation') is-invalid @enderror" name="password-confirmation" placeholder="Confirmar Contraseña" required>
                        </div>
                    </div>
                    <div class="col-12 form-row mt-4">
                        <div class="form-check ml-auto">
                          <input class="form-check-input" type="checkbox" name="policy-privacy" id="defaultCheck1" required>
                          <label class="form-check-label" for="defaultCheck1">
                              {{__('He leído y acepto la ')}}
                            <a href="#" class="text-decoration-none" data-toggle="modal" data-target="#privacy-policy">pólitica de privacidad</a>
                          </label>
                        </div>
                    </div>
                    <div class="col-12 text-right my-5">
                        <a href="{{url('/')}}" class="btn btn-link text-dark text-decoration-none">
                            {{__('CANCELAR')}}
                        </a>
                        <button type="submit" class="btn btn-login rounded-0">
                            {{__('REGISTRARSE')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('privacy-policy')
@endsection

<div class="modal fade" id="create" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modals">
            <div class="modal-header">
                <h5 class="modal-title">NUEVO GUISADO</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('stews.store')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control create-input" name="name" placeholder="Nombre del guisado" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link text-white text-decoration-none" data-dismiss="modal">{{__('CANCELAR')}}</button>
                    <button type="submit" class="btn btn-submit rounded-0" name="button">{{__('ACEPTAR')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

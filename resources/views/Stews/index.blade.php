@extends('layouts.app')
@section('attendance', 'side-active')
@section('top-title','GUISADOS')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 px-4 form-row mt-4">
                <div class="col-lg-6 mb-3">
                    <form class="form-row" action="" method="get">
                        <div class="form-group col-11 col-md-8 mb-0 pr-0">
                            <input type="text" class="form-control search-input" name="search" value="{{$search}}" placeholder="Buscar...">
                        </div>
                        <button type="submit" class="btn btn-search col-1">
                            <i class="fas fa-search"></i>
                        </button>
                    </form>
                </div>

                <div class="col-lg-6 text-right px-0">
                    <a href="#" data-toggle="modal" data-target="#create" class="btn btn-new-entry px-4 rounded-0">{{__('NUEVO REGISTRO')}}</a>
                </div>
            </div>

            @if($stews->count())
                <div class="col-12 table-responsive mt-4">
                    <table class="table">
                        <thead>
                            <tr class="text-center">
                                <th>{{__('ID')}}</th>
                                <th>{{__('Nombre')}}</th>
                                <th>{{__('Estatus')}}</th>
                                <th>{{__('Acciones')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($stews as $stew)
                                <tr class="text-center">
                                    <td class="align-middle text-truncate">{{ $stew->id }}</td>
                                    <td class="align-middle text-truncate">{{ $stew->name }}</td>
                                    <td class="align-middle text-truncate text-dark {{ $stew->status ? 'table-success' : 'table-danger' }}">
                                        @if($stew->status)
                                            {{__('Disponible')}}
                                        @else
                                            {{__('No Disponible')}}
                                        @endif
                                    </td>
                                    <td class="align-middle text-truncate">
                                        @if($stew->status)
                                            <form class="" action="{{ route('stews.disable',['id' => $stew->id]) }}" method="post">
                                                @csrf
                                                <button type="submit" class="btn btn-link btn-actions" name="button" data-toggle="tooltip" data-placement="left" data-title="Desactivar">
                                                    <i class="fad fa-times fa-lg"></i>
                                                </button>
                                            </form>
                                        @else
                                            <form class="" action="{{ route('stews.enable',['id' => $stew->id]) }}" method="post">
                                                @csrf
                                                <button type="submit" class="btn btn-link btn-actions" name="button" data-toggle="tooltip" data-placement="left" data-title="Activar">
                                                    <i class="fad fa-check-square fa-lg"></i>
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="col-12 text-center mt-5 no-records">
                    <i class="fad fa-exclamation-triangle fa-5x mb-3"></i>
                    <h3>{{__('SIN RESULTADOS')}}</h3>
                </div>
            @endif
        </div>
    </div>

    @include('Stews.create');

@endsection

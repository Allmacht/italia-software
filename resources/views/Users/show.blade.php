@extends('layouts.app')
@section('lista-personal', 'side-active')
@section('top-title','PERSONAL - '.$user->names)
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mt-4">
                <div class="col-xl-4 float-left mb-3 img-sticky">
                    <div class="col-12 text-center">
                        @if(is_null($user->image))
                            <img src="{{('https://api.adorable.io/avatars/300/').$user->email}}" alt="" class="img-fluid img-profile">
                        @else
                            <img src="{{asset('images/users_images/'.$user->image)}}" alt="" class="fluid img-profile">
                        @endif
                    </div>
                    <div class="col-12 text-center mt-3 py-2">
                        @foreach ($user->getRoleNames() as $role)
                            <span class="badge badge-info">{{$role}}</span>
                        @endforeach
                    </div>
                    <div class="col-12 text-center mt-3">
                        <a href="#" class="btn btn-danger btn-show px-3" data-toggle="tooltip" data-placement="left" data-title="PDF">
                            <i class="fad fa-file-pdf"></i>
                        </a>
                        <a href="{{route('users.edit', ['id' => $user->id])}}" class="btn btn-danger btn-show" data-toggle="tooltip" data-placement="right" data-title="Editar">
                            <i class="fad fa-user-edit"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-8 float-right table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="text-center">
                                <th>{{__('CLAVE')}}</th>
                                <th>{{__('VALOR')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <h5 class="color-text mb-0 py-3">
                                        <i class="fad fa-user-lock mr-2"></i>
                                        {{__('Información de usuario')}}
                                    </h5>
                                </td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('ID')}}</b></td>
                                <td>{{$user->id}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Nombre de usuario')}}</b></td>
                                <td>{{$user->name}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Correo electónico')}}</b></td>
                                <td>{{$user->email}}</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h5 class="color-text mb-0 py-3">
                                        <i class="fad fa-user mr-2"></i>
                                        {{__('Información de personal')}}
                                    </h5>
                                </td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Nombre(s)')}}</b></td>
                                <td>{{$user->names}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Apellido paterno')}}</b></td>
                                <td>{{$user->paternal_surname}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Apellido materno')}}</b></td>
                                <td>{{$user->maternal_surname}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Sexo')}}</b></td>
                                <td>{{$user->gender}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Fecha de nacimiento')}}</b></td>
                                <td>{{$user->birthdate}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Teléfono')}}</b></td>
                                <td>{{$user->phone}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Nacionalidad')}}</b></td>
                                <td>{{$user->nationality}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Estado civil')}}</b></td>
                                <td>{{$user->marital_status}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('N° seguro social')}}</b></td>
                                <td>{{$user->medical_insurance}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('RFC')}}</b></td>
                                <td>{{$user->rfc}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Contacto de emergencía')}}</b> </td>
                                <td>{{$user->emergency_contact}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Teléfono de contacto')}}</b></td>
                                <td>{{$user->contact_number}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Medicamento')}}</b> </td>
                                <td>{{$user->medication}}</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h5 class="color-text mb-0 py-3">
                                        <i class="fad fa-map-marker-alt mr-2"></i>
                                        {{__('Domicilio')}}
                                    </h5>
                                </td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Calle')}}</b></td>
                                <td>{{$user->street}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Colonia/Fraccionamiento')}}</b></td>
                                <td>{{$user->neighborhood}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Número exterior')}}</b></td>
                                <td>{{$user->external_number}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Número interior')}}</b></td>
                                <td>{{$user->internal_number}}</td>
                            </tr>
                            <tr class="text-center">
                                <td><b>{{__('Código postal')}}</b></td>
                                <td>{{$user->zipcode}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.app')
@section('lista-personal', 'side-active')
@section('top-title','LISTA DE PERSONAL')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 px-4 form-row mt-4">
                <div class="col-lg-6 mb-3">
                    <form class="form-row" action="" method="get">
                        <div class="form-group col-11 col-md-8 mb-0 pr-0">
                            <input type="text" class="form-control search-input" name="search" value="{{$search}}" placeholder="Buscar...">
                        </div>
                        <button type="submit" class="btn btn-search col-1">
                            <i class="fas fa-search"></i>
                        </button>
                    </form>
                </div>

                <div class="col-lg-6 text-right px-0">
                    <a href="#" class="btn btn-new-entry rounded-0" data-toggle="tooltip" data-placement="left" data-title="Imprimir PDF">
                        <i class="fas fa-file-pdf"></i>
                    </a>
                    <a href="#" class="btn btn-new-entry px-4 rounded-0">{{__('NUEVO REGISTRO')}}</a>
                </div>
            </div>
            @if($users->count())
                <div class="col-12 table-responsive mt-4">
                    <table class="table">
                        <thead>
                            <tr class="text-center">
                                <th scope="col">#</th>
                                <th>{{__('NOMBRE')}}</th>
                                <th>{{__('ESTATUS')}}</th>
                                <th>{{__('ACCIONES')}}</th>
                            </tr>
                        </thead>
                        @foreach ($users as $user)
                            <tr class="text-center">
                                <td scope="row" class="align-middle text-truncate">
                                    @if(is_null($user->image))
                                        <img src="{{('https://api.adorable.io/avatars/40/').$user->email}}" alt="" class="img-fluid" style="border-radius: 50%">
                                    @else
                                        <img src="{{asset('images/users_images/'.$user->image)}}" alt="" class="img-fluid" style="border-radius: 50%; width: 40px; height:40px" >
                                    @endif
                                </td>
                                <td class="align-middle text-truncate">
                                    {{$user->names." ".$user->paternal_surname." ".$user->maternal_surname}}
                                </td>
                                @if ($user->authorized)
                                    <td class="align-middle text-truncate {{$user->status ? 'table-success' : 'table-danger'}} text-dark">
                                        {{$user->status ? 'ACTIVO' : 'DESACTIVADO'}}
                                    </td>
                                @else
                                    <td class="align-middle text-truncate {{$user->authorized ? 'table-success' : 'table-warning'}} text-dark">
                                        {{$user->authorized ? 'ACTIVO' : 'PENDIENTE'}}
                                    </td>
                                @endif
                                <td class="align-middle text-truncate">
                                    <a href="{{route('users.roles',['id' => $user->id])}}" class="btn btn-link btn-actions" data-toggle="tooltip" data-placement="left" data-title="Roles">
                                        <i class="fad fa-user-shield fa-lg"></i>
                                    </a>
                                    <a href="{{route('users.show', ['id' => $user->id])}}" class="btn btn-link btn-actions" data-toggle="tooltip" data-placement="top" data-title="Información">
                                        <i class="fad fa-user-tag fa-lg"></i>
                                    </a>
                                    @if($user->authorized)
                                        @if ($user->status)
                                            <span class="open-modal" data-toggle="modal" data-target="#disable" data-action="disable" data-id="{{$user->id}}">
                                                <button type="button" class="btn btn-link btn-actions" data-toggle="tooltip" data-placement="top" data-title="Desactivar">
                                                    <i class="fad fa-user-slash fa-lg"></i>
                                                </button>
                                            </span>
                                        @else
                                            <span class="open-modal" data-toggle="modal" data-target="#enable" data-action="enable" data-id="{{$user->id}}">
                                                <button type="button" class="btn btn-link btn-actions" data-toggle="tooltip" data-placement="top" data-title="Activar">
                                                    <i class="fad fa-user-check fa-lg"></i>
                                                </button>
                                            </span>
                                        @endif
                                    @else
                                        <span class="open-modal" data-toggle="modal" data-target="#authorize" data-action="authorize" data-id="{{$user->id}}">
                                            <button type="button" class="btn btn-link btn-actions" data-toggle="tooltip" data-placement="top" data-title="Autorizar">
                                                <i class="fad fa-shield-check fa-lg"></i>
                                            </button>
                                        </span>
                                    @endif


                                    <a href="{{route('users.edit', ['id' => $user->id])}}" class="btn btn-link btn-actions" data-toggle="tooltip" data-placement="top" data-title="Editar">
                                        <i class="fad fa-user-edit fa-lg"></i>
                                    </a>

                                    <span class="open-modal" data-toggle="modal" data-target="#delete" data-action="delete" data-id="{{$user->id}}">
                                        <button type="button" class="btn btn-link btn-actions" data-toggle="tooltip" data-placement="right" data-title="Eliminar">
                                            <i class="fad fa-user-times fa-lg"></i>
                                        </button>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            @else
                <div class="col-12 text-center mt-5 no-records">
                    <i class="fad fa-exclamation-triangle fa-5x mb-3"></i>
                    <h3>{{__('SIN RESULTADOS')}}</h3>
                </div>
            @endif
        </div>
    </div>

    @include('Users.disable')
    @include('Users.authorize')
    @include('Users.enable')
    @include('Users.delete')

@endsection
@section('scripts')
    <script src="{{asset('js/getID.js')}}" charset="utf-8"></script>
    <script type="text/javascript">
        $('#btn-authorize').click(function(){
            $(this).empty().append("<i class='fad fa-spinner fa-pulse mr-3'></i>CARGANDO...");
        });
    </script>
@endsection

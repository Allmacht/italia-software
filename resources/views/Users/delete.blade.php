<div class="modal fade" id="delete" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modals">
            <div class="modal-header">
                <h5 class="modal-title">{{__('ATENCIÓN')}}</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('users.destroy')}}" method="post">
                @csrf
                <div class="modal-body text-center">
                    <p>{{__('¿Realmente desea eliminar este elemento?')}}</p>
                    <p class="text-danger">{{__('Esta acción no puede revertirse')}}</p>
                    <input type="hidden" name="id" id="delete-id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-cancelar text-decoration-none" data-dismiss="modal">
                        {{__('CANCELAR')}}
                    </button>
                    <button type="submit" class="btn btn-aceptar">
                        {{__('ACEPTAR')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

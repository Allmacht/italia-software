<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>

    @php
    $urlImage = "https://scontent.fgdl4-1.fna.fbcdn.net/v/t1.0-9/23472905_1529881200430444_7055956152128889880_n.jpg?_nc_cat=111&_nc_ohc=8-xmyoVAW3kAQmqAUz1-S5ibQPYFzCzrFWu32h05uP93wRlmiZh_M9R3w&_nc_ht=scontent.fgdl4-1.fna&oh=8e1e18541518a029c6d5646a6b03d657&oe=5E7793A7";
    @endphp

    <body
        style="
        font-family: 'Ubuntu', sans-serif;
        margin: 0;
        height: 100%;
        hyphens: auto;
        line-height: 1.4;
        margin: 0;
        -moz-hyphens: auto;
        -ms-word-break: break-all;
        width: 100% !important;
        -webkit-hyphens: auto;
        -webkit-text-size-adjust: none;
        word-break: break-word;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;">

        <div id="container" style="">

            <div id="row" style="
                ">

                <div id="col-12" style="

                    width: 100%;
                    padding-right: 15px;
                    padding-left: 15px;
                    width: 100%;
                    padding-top: 35px;
                    background-color: #eee;
                    ">
                </div>
                <div id="col-12" style="

                    width: 100%;
                    text-align: center !important;
                    padding-right: 15px;
                    padding-left: 15px;">

                    <img src="{{$urlImage}}" alt="" width="500px"  style="max-width: 100%;height: auto;">
                </div>
                <div id="col-12" style="

                    width: 100%;
                    text-align: center !important;
                    padding-right: 15px;
                    padding-left: 15px;
                    background-color: #eee;
                    padding-top: 50px;
                    padding-bottom: 30px;">

                    <h2>Hola!, {{$data->names}}</h2>
                    <p>Tu cuenta ha sido autorizada por un Administrador, ahora puedes iniciar sesión en el sistema.</p>
                    <a href="{{route('login')}}" style="
                        text-decoration: none !important;
                        display: inline-block;
                        font-weight: 400;
                        color: #212529;
                        text-align: center;
                        vertical-align: middle;
                        -webkit-user-select: none;
                           -moz-user-select: none;
                            -ms-user-select: none;
                                user-select: none;
                        background-color: transparent;
                        border: 1px solid transparent;
                        border-style: solid;
                        border-top-color: green;
                        border-bottom-color: green;
                        border-left-color: green;
                        border-right-color: green;
                        border-radius: 0;
                        padding: 0.375rem 0.75rem;
                        font-size: 1rem;
                        line-height: 1.5;
                        color: green;
                        -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                        transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
                    ">INICIAR SESIÓN</a>
                </div>
            </div>
        </div>
    </body>
</html>

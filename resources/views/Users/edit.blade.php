@extends('layouts.app')
@section('lista-personal', 'side-active')
@section('top-title','EDITAR PERSONAL')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/edit.css')}}">
@endsection
@section('content')

    <div class="container-fluid">
        <div class="row">
            <form class="col-12 mt-4" action="{{route('users.update',['id' => $user->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-12 form-row">
                    <div class="col-12 mb-4">
                        <h4 class="color-text">
                            <i class="fad fa-user-lock mr-2"></i>
                            {{__('Información personal')}}
                        </h4>
                    </div>
                    <div class="form-group col-xl-4 mb-4">
                        <label for="names" class="color-text">{{__('Nombre(s)')}}</label>
                        <input type="text" class="form-control @error('names') is-invalid @enderror" name="names" value="{{$user->names}}" placeholder="Nombre(s)" required>
                    </div>
                    <div class="form-group col-xl-4 mb-4">
                        <label for="paternal_surname" class="color-text">{{__('Apellido paterno')}}</label>
                        <input type="text" class="form-control @error('paternal_surname') is-invalid @enderror" name="paternal_surname" value="{{$user->paternal_surname}}" placeholder="Apellido paterno" required>
                    </div>
                    <div class="form-group col-xl-4 mb-4">
                        <label for="maternal_surname" class="color-text">{{__('Apellido materno')}}</label>
                        <input type="text" class="form-control @error('maternal_surname') is-invalid @enderror" name="maternal_surname" value="{{$user->maternal_surname}}" placeholder="Apellido materno" required>
                    </div>
                    <div class="form-group col-xl-2 mb-4">
                        <label for="birthdate" class="color-text">{{__('Fecha de nacimiento')}}</label>
                        <input type="date" class="form-control @error('birthdate') is-invalid @enderror" name="birthdate" value="{{$user->birthdate}}" required>
                    </div>
                    <div class="form-group col-xl-4 mb-4">
                        <label for="nationality" class="color-text">{{__('Nacionalidad')}}</label>
                        <input type="text" class="form-control @error('nationality') is-invalid @enderror" name="nationality" value="{{$user->nationality}}" placeholder="Nacionalidad" required>
                    </div>
                    <div class="form-group col-xl-3 mb-4">
                        <label for="marital_status" class="color-text">{{__('Estado civil')}}</label>
                        <select class="form-control form-select" name="marital_status" required>
                            <option value="" disabled selected>{{__('Estado civil')}}</option>
                            <option value="Casado/a" @if($user->marital_status == 'Casado/a') selected @endif>{{__('Casado/a')}}</option>
                            <option value="Soltero/a" @if($user->marital_status == 'Soltero/a') selected @endif>{{__('Soltero/a')}}</option>
                        </select>
                    </div>
                    <div class="form-group col-xl-3 mb-4">
                        <label for="gender" class="color-text">{{__('Sexo')}}</label>
                        <select class="form-control form-select" name="gender">
                            <option value="" disabled selected>{{__('Sexo')}}</option>
                            <option value="Masculino" @if($user->gender == 'Masculino') selected @endif>{{__('Masculino')}}</option>
                            <option value="Femenino" @if($user->gender == 'Femenino') selected @endif>{{__('Femenino')}}</option>
                        </select>
                    </div>
                    <div class="form-group col-xl-4 mb-4">
                        <label for="phone" class="color-text">{{__('Teléfono')}}</label>
                        <input type="number" class="form-control @error('phone') is-invalid @endif" name="phone" min="1" max="9999999999" value="{{$user->phone}}" placeholder="Teléfono" required>
                    </div>
                    <div class="form-group col-xl-4 mb-4">
                        <label for="medical_insurance" class="color-text">{{__('N° seguro social')}}</label>
                        <input type="text" class="form-control @error('medical_insurance') is-invalid @enderror" name="medical_insurance" value="{{$user->medical_insurance}}" placeholder="N° seguro social" required>
                    </div>
                    <div class="form-group col-xl-4 mb-4">
                        <label for="rfc" class="color-text">{{__('R.F.C.')}}</label>
                        <input type="text" class="form-control @error('rfc') is-invalid @enderror" name="rfc" value="{{$user->rfc}}" placeholder="R.F.C." required>
                    </div>
                    <div class="form-group col-xl-4 mb-4">
                        <label for="emergency_contact" class="color-text">{{__('Contacto de emergencía')}}</label>
                        <input type="text" class="form-control @error('emergency_contact') is-invalid @enderror" name="emergency_contact" value="{{$user->emergency_contact}}" placeholder="Contacto de emergencía" required>
                    </div>
                    <div class="form-group col-xl-3 mb-4">
                        <label for="contact_number" class="color-text">{{__('Teléfono de emergencía')}}</label>
                        <input type="number" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" value="{{$user->contact_number}}" placeholder="Teléfono de emergencía" required>
                    </div>
                    <div class="form-group col-xl-5 mb-4">
                        <label for="medication" class="color-text">{{__('Medicamento controlado')}}</label>
                        <input type="text" class="form-control @error('medication') is-invalid @enderror" name="medication" value="{{$user->medication}}" placeholder="Medicamento controlado">
                    </div>
                </div>
                <div class="col-12 form-row">
                    <div class="col-12 mb-4 py-3">
                        <h4 class="color-text">
                            <i class="fad fa-map-marker-alt mr-2"></i>
                            {{__('Domicilio')}}
                        </h4>
                    </div>
                    <div class="form-group col-xl-4 mb-4">
                        <label for="street" class="color-text">{{__('Calle')}}</label>
                        <input type="text" class="form-control @error('street') is-invalid @enderror" name="street" value="{{$user->street}}" placeholder="Calle" required>
                    </div>
                    <div class="form-group col-xl-4 mb-4">
                        <label for="neighborhood" class="color-text">{{__('Colonia / Fraccionamiento')}}</label>
                        <input type="text" class="form-control @error('neighborhood') is-invalid @enderror" name="neighborhood" value="{{$user->neighborhood}}" placeholder="Colonia / Fraccionamiento">
                    </div>
                    <div class="form-group col-xl-2 mb-4">
                        <label for="external_number" class="color-text">{{__('Número exterior')}}</label>
                        <input type="number" class="form-control @error('external_number') is-invalid @enderror" name="external_number" value="{{$user->external_number}}" placeholder="Número exterior" required>
                    </div>
                    <div class="form-group col-xl-2 mb-4">
                        <label for="internal_number" class="color-text">{{__('Número interior')}}</label>
                        <input type="text" class="form-control @error('internal_number') is-invalid @enderror" name="internal_number" value="{{$user->internal_number}}" placeholder="Número interior">
                    </div>
                    <div class="form-group col-xl-4 mb-4">
                        <label for="zipcode" class="color-text">{{__('Código postal')}}</label>
                        <input type="number" class="form-control @error('zipcode') is-invalid @enderror" name="zipcode" value="{{$user->zipcode}}" placeholder="Código postal" required>
                    </div>
                </div>
                <div class="col-12 px-1 clearfix">
                    <div class="col-12 mb-4 py-3">
                        <h4 class="color-text">
                            <i class="fad fa-user-lock mr-2"></i>
                            {{__('Usuario')}}
                        </h4>
                    </div>
                    <div class="col-xl-4 mb-4 float-left">
                        <div class="col-12 text-center">
                            <div class="col-12 mb-3">
                                @if(is_null($user->image))
                                    <img id="image-preview" src="{{('https://api.adorable.io/avatars/300/').$user->email}}" alt="" class="img-fluid img-profile">
                                @else
                                    <img  src="{{asset('')}}" alt="" >
                                    <img id="image-preview" src="{{asset('/images/users_images/'.Auth::user()->image)}}" alt="" class="img-fluid img-profile">
                                @endif
                            </div>
                            <div class="col-12">
                                <button type="button" class="btn btn-submit rounded-0" data-toggle="modal" data-target="#modal-image" name="button">{{__('IMAGEN')}}</button>
                            </div>

                            @include('Users.modal-image')
                        </div>
                    </div>
                    <div class="col-xl-8 form-row float-right">
                        <div class="from-group col-xl-6 mb-4">
                            <label for="name" class="color-text">{{__('Nombre de usuario')}}</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$user->name}}" placeholder="Nombre de usuario" required>
                        </div>
                        <div class="form-group col-xl-6 mb-4">
                            <label for="email" class="color-text">{{__('Correo electrónico')}}</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->email}}" placeholder="Correo electrónico" required>
                        </div>
                        <div class="form-group col-xl-6 mb-4">
                            <label for="salary" class="color-text">{{__('Salario')}}</label>
                            <input type="number" step="0.1" min="1" name="salary" class="form-control @error('salary') is-invalid @enderror" value="{{$user->salary}}" placeholder="Salario">
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 text-center mb-5">
                    <a href="{{route('users.index')}}" class="btn btn-link text-white text-decoration-none">{{__('CANCELAR')}}</a>
                    <button type="submit" class="btn btn-submit rounded-0" name="button">{{__('ACTUALIZAR')}}</button>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('scripts')
    <script type="text/javascript">
        $('#user-image').change(function(e){
            $('#image-name').empty().append(e.target.files[0].name);
            readURL(this);
        });

        function readURL(input) {
           if (input.files && input.files[0]) {
             var reader = new FileReader();

             reader.onload = function(e) {
               $('#image-preview').empty().attr('src', e.target.result);
             }

             reader.readAsDataURL(input.files[0]);
           }
         }

    </script>
@endsection

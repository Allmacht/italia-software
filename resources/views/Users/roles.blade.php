@extends('layouts.app')
@section('lista-personal', 'side-active')
@section('top-title','ROLES')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
    <link rel="stylesheet" href="{{asset('css/layouts/edit.css')}}">
@endsection
@section('content')

    <user-roles v-bind:user="{{ json_encode($user) }}" v-bind:user_roles="{{ json_encode($roles) }}"></user-roles>

@endsection

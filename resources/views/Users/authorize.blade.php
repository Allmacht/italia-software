<div class="modal fade" tabindex="-1" id="authorize" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modals">
            <div class="modal-header">
                <h5 class="modal-title">{{__('ATENCIÓN')}}</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('users.authorize')}}" method="post">
                @csrf
                <div class="modal-body text-center">
                    <p>{{__('¿Realmente desea autorizar la cuenta?')}}</p>
                    <input type="hidden" name="id" id="authorize-id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-cancelar text-decoration-none" data-dismiss="modal">
                        {{__('CANCELAR')}}
                    </button>
                    <button type="submit" class="btn btn-aceptar" id="btn-authorize">
                        {{__('ACEPTAR')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

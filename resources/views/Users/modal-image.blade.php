<div class="modal fade" id="modal-image" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modals">
            <div class="modal-header">
                <h5 class="modal-title">{{__('CAMBIAR IMAGEN')}}</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-center">{{__('Cambiar imagen de usuario')}}</p>
                <div class="input-group mb-3">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="user-image" name="user-image">
                    <label class="custom-file-label" for="user-image" id="image-name" aria-describedby="user-image">{{__('Elegir imagen')}}</label>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link text-white text-decoration-none" data-dismiss="modal">{{__('CANCELAR')}}</button>
                <button type="button" class="btn btn-submit rounded-0" name="button" data-dismiss="modal">{{__('ACEPTAR')}}</button>
            </div>
        </div>
    </div>
</div>

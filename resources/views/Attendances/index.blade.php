@extends('layouts.app')
@section('attendance', 'side-active')
@section('top-title','LISTA DE PERSONAL')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 px-4 form-row mt-4">
                <div class="col-lg-6 mb-3">
                    <form class="form-row" action="" method="get">
                        <div class="form-group col-11 col-md-8 mb-0 pr-0">
                            <input type="text" class="form-control search-input" name="search" value="{{$search}}" placeholder="Buscar...">
                        </div>
                        <button type="submit" class="btn btn-search col-1">
                            <i class="fas fa-search"></i>
                        </button>
                    </form>
                </div>

                <div class="col-lg-6 text-right px-0">
                    <a href="{{route('attendances.registers')}}" class="btn btn-new-entry rounded-0">
                        REGISTROS
                    </a>
                    <button data-toggle="modal" data-target="#attendance" class="btn btn-new-entry px-4 rounded-0">{{__('REGISTRAR ENTRADA')}}</button>
                </div>
            </div>

            @if($attendances->count())

                <div class="col-12 table-responsive mt-4">
                    <table class="table">
                        <thead>
                            <tr class="text-center">
                                <th>{{__('Usuario')}}</th>
                                <th>{{__('Entrada')}}</th>
                                <th>{{__('Salida')}}</th>
                                <th>{{__('Retardo')}}</th>
                                <th>{{__('Acciones')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($attendances as $attendance)
                                <tr class="text-center">
                                    <td class="align-middle text-truncate">{{$attendance->user->names}}</td>
                                    <td class="align-middle text-truncate">{{$attendance->entry}}</td>
                                    <td class="align-middle text-truncate">{{$attendance->departure ?? 'PENDIENTE'}}</td>
                                    <td class="align-middle text-truncate @if($attendance->difference != 'SIN RETARDO') table-danger text-dark @endif">{{$attendance->difference}}</td>
                                    <td class="align-middle text-truncate">
                                        @if($attendance->departure == null)
                                            <span class="open-modal" data-toggle="modal" data-target="#departiture" data-action="departiture" data-id="{{$attendance->id}}">
                                                <button type="button" class="btn btn-link btn-actions" name="button" data-toggle="tooltip" data-placement="right" data-title="Registrar salida">
                                                    <i class="fad fa-user-tag fa-lg"></i>
                                                </button>
                                            </span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            @endif
        </div>
    </div>

    @include('Attendances.attendance')
    @include('Attendances.departiture')

@endsection
@section('scripts')
    <script src="{{asset('js/getID.js')}}" charset="utf-8"></script>
@endsection

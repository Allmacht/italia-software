<div class="modal fade" id="departiture" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modals">
            <div class="modal-header">
                <h5 class="modal-title">{{__('ATENCIÓN')}}</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('attendances.departiture')}}" method="post">
                @csrf
                <div class="modal-body text-center">
                    <input type="hidden" name="id" id="attendance-id" value="">
                    <p>{{__('¿Realmente desea registrar la hora de salida?')}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-cancelar text-decoration-none" data-dismiss="modal">
                        {{__('CANCELAR')}}
                    </button>
                    <button type="submit" class="btn btn-aceptar">
                        {{__('ACEPTAR')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

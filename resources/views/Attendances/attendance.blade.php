<div class="modal fade" id="attendance" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modals">
            <div class="modal-header">
                <h5 class="modal-title">{{__('REGISTRO DE ENTRADA')}}</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="" action="{{ route('attendances.store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <select class="form-control form-select" name="user_id" required>
                            <option value="" selected disabled>{{__('Seleccione un usuario')}}</option>
                            @foreach ($users as $user)
                                <option value="{{$user->id}}">{{$user->names}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link text-white text-decoration-none" data-dismiss="modal">{{__('CANCELAR')}}</button>
                    <button type="submit" class="btn btn-submit rounded-0" name="button">{{__('ACEPTAR')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

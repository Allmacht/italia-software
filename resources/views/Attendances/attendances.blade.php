@extends('layouts.app')
@section('attendance', 'side-active')
@section('top-title','LISTA DE PERSONAL')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
    <link rel="stylesheet" href="{{asset('css/layouts/edit.css')}}">

@endsection
@section('content')

    <attendances></attendances>

@endsection

<div class="modal fade" id="logout" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modals">
            <div class="modal-header">
                <h5 class="modal-title">{{__('ATENCIÓN')}}</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <p>{{__('¿Realmente desea cerrar sesión?')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-cancelar text-decoration-none" data-dismiss="modal">
                    {{__('CANCELAR')}}
                </button>
                <form action="{{route('logout')}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-aceptar">
                        {{__('ACEPTAR')}}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

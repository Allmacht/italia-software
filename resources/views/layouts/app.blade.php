<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title','Italia software')</title>

    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{asset('js/all.min.js')}}" charset="utf-8"></script>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @auth
        <link rel="stylesheet" href="{{asset('css/layouts/app.css')}}">
    @endauth
    @yield('styles')
</head>
<body>
    <div id="app">
        @guest
            <nav class="navbar navbar-expand-md navbar-light bg-white shadow sticky-top">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ __('ITALIA') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#guestContent" aria-controls="guestContent" aria-expanded="false">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="guestContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}"><i class="fad fa-sign-in-alt mr-2"></i>{{ __(' Iniciar sesión') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}"><i class="fad fa-user-plus mr-2"></i>{{ __(' Crear cuenta') }}</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @else

            <div class="sidenav shadow" id="sidenav">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 text-right sticky-top">
                            <button type="button" class="btn btn-link button-menu">
                                <i class="fas fa-times fa-lg icon-close"></i>
                            </button>
                        </div>
                        <div class="col-12 text-center top-div">
                            <img src="{{asset('images/logo-white.jpg')}}" class="img-fluid mt-3" width="400px" alt="">
                        </div>

                        <div class="col-12 mt-4 px-0 list-options">
                            <a href="{{route('home')}}" class="side-link @yield('dashboard') text-decoration-none">
                                <i class="far fa-object-group fa-lg mr-3"></i>
                                {{__('DASHBOARD')}}
                            </a>
                            {{-- MENU DEL PERSONAL --}}
                            <a href="#" class="side-link dropdown-btn text-decoration-none" data-target="#dropdown-inv">
                                <i class="fad fa-users fa-lg mr-3"></i>
                                {{__('PERSONAL')}}
                                <i class="fa fa-caret-down ml-5"></i>
                            </a>
                            <div class="dropdown-container hide" id="dropdown-inv">
                                <a href="{{route('users.index')}}" class="side-link link-dropdown @yield('lista-personal') text-decoration-none">
                                    {{ __('LISTA DE PERSONAL') }}
                                </a>
                                <a href="{{route('attendances.index')}}" class="side-link link-dropdown @yield('attendance') text-decoration-none">
                                    {{__('ASISTENCIAS')}}
                                </a>
                            </div>

                            <a href="{{route('stews.index')}}" class="side-link @yield('stews') text-decoration-none">
                                <i class="fad fa-utensils fa-lg mr-3"></i>
                                {{__('GUISADOS')}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        @endguest

        <main>
            @auth
                <nav class="navbar navbar-dark bg-dark button-nav sticky-top main-navbar navbar-expand">

                    <div class="collapse navbar-collapse">
                        <ul class="navbar-nav mr-auto">
                            <button type="button" class="btn btn-link navbar-brand button-menu">
                                <i class="fas fa-bars fa-lg"></i>
                            </button>
                            <span class="navbar-brand mr-auto">@yield('top-title')</span>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link dropdown-toggle" role="button" id="main-navbar-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(is_null(Auth::user()->image))
                                        <img src="{{('https://api.adorable.io/avatars/40/').Auth::user()->email}}" alt="" class="img-fluid" style="border-radius: 50%">
                                    @else
                                        <img src="{{asset('/images/users_images/'.Auth::user()->image)}}" alt="" class="img-fluid" style="border-radius: 50%; width:40px; height:40px">
                                    @endif
                                </a>
                                <div class="dropdown-menu dropdown-menu-right text-center rounded-0 dropdown-user shadow py-0">
                                    <div class="user-info py-3">
                                        @if(is_null(Auth::user()->image))
                                            <img src="{{('https://api.adorable.io/avatars/120/').Auth::user()->email}}" alt="" class="img-fluid" style="border-radius: 50%">
                                        @else
                                            <img src="{{asset('/images/users_images/'.Auth::user()->image)}}" alt="" class="img-fluid" style="border-radius: 50%; width:120px; height:120px">
                                        @endif
                                        <h4 class="mt-3 text-white">{{Auth::user()->name}}</h4>
                                        @foreach (Auth::user()->getRoleNames() as $role)
                                            <span class="badge badge-pill badge-info">{{$role}}</span>
                                        @endforeach
                                    </div>
                                    <div class="text-center">
                                        <a href="#" class="dropdown-item text-white py-3 dropdown-user-button">
                                            <strong>
                                                {{__('Mí perfil')}}
                                            </strong>
                                        </a>

                                        <button type="button" data-toggle="modal" data-target="#logout" class="dropdown-item text-white py-3 dropdown-user-button">
                                            <strong>
                                                {{__('Cerrar sesión')}}
                                            </strong>
                                        </button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            @endauth
            @yield('content')
        </main>
    </div>

    @include('Notifications')
    @include('layouts.logout')

    <script src="{{asset('js/jquery-3.4.1.min.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/popper.min.js')}}" charset="utf-8"></script>
    @auth
        <script src="{{asset('js/sidebar.js')}}" charset="utf-8"></script>
    @endauth
    <script src="{{asset('js/TweenMax.min.js')}}" charset="utf-8"></script>
    <script src="{{asset('js/notifications.js')}}" charset="utf-8"></script>
    @yield('scripts')
</body>
</html>

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->status == false):
            Auth::logout();
            return redirect()->route('login')->withErrors('La cuenta ha sido desactivada');
        endif;

        if(Auth::user()->authorized == false):
            Auth::logout();
            return redirect()->route('login')->withErrors('La cuenta no ha sido verificada por un administrador');
        endif;
        
        return $next($request);
    }
}

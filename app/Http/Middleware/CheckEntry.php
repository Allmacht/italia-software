<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Attendance;
use Illuminate\Support\Facades\Auth;

class CheckEntry
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->hasAnyRole(['Administrador','super-admin']))
            return $next($request);

        $entry = Attendance::where('date','=',Carbon::now()->format('YYYY-MM-DD'))
                    ->whereUser_id(Auth::user()->id)
                    ->whereStatus(true)
                    ->first();

        if(is_null($entry)):
            Auth::logout();
            return redirect()->route('login')->withErrors('El Administrador debe registrar su hora de entrada');
        endif;
        
        return $next($request);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Attendance;
use App\User;
use Carbon\Carbon;

class AttendanceController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $date = Carbon::now()->isoFormat('YYYY-MM-DD');
        $attendances = Attendance::select('*','attendances.id as id')
                            ->join('users','attendances.user_id','users.id')
                            ->where(DB::raw("CONCAT(names,' ',paternal_surname,' ',maternal_surname)"),'like',"%$search%")
                            ->where('attendances.date',$date)
                            ->get();

        foreach ($attendances as $attendance) {
            $start = new Carbon('07:00:00');
            if(Carbon::parse($attendance->entry)->gt($start)){
                $difference = $start->diff($attendance->entry)->format('%H:%I:%S');
            }else{
                $difference = 'SIN RETARDO';
            }
            $attendance->difference = $difference;
        }
        $users = User::whereStatus(true)->whereAuthorized(true)->get();

        return view('Attendances.index', compact('attendances','search','users'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required|exists:users,id'
        ],[
            'user_id.required' => 'El usuario es requerido',
            'user_id.exists'   => 'El usuario seleccionado no existe'
        ]);

        $now = Carbon::now()->isoFormat('YYYY-MM-DD');
        $verify = Attendance::where('date','=',$now)->whereUser_id($request->user_id)->whereStatus(true)->first();
        if(!is_null($verify))
            return redirect()->route('attendances.index')->withErrors('La entrada del usuario seleccionado ya fue registrada');

        $attendace = new Attendance();
        $attendace->user_id = $request->user_id;
        $attendace->date    = $now;
        $attendace->entry   = Carbon::now()->format('H:i:s');
        $attendace->status  = true;

        $attendace->save();

        return redirect()->route('attendances.index')->withStatus('Entrada registrada correctamente');
    }

    public function departiture(Request $request)
    {
        $attendance = Attendance::findOrfail($request->id);
        $attendance->departure = Carbon::now()->format('H:i:s');
        $attendance->status = false;

        $attendance->update();
        return redirect()->route('attendances.index')->withStatus('Salida registrada correctamente');
    }


    public function registers(Request $request)
    {
        if($request->ajax()):
            $attendances = Attendance::where('date','>=',$request->initial_date)->where('date','<=',$request->final_date)->with(['user'])->get();
            return response()->json(['attendances' => $attendances],200);
        endif;
        return view('Attendances.attendances');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use App\Mail\AuthorizedAccount;
use App\User;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $users = User::where(DB::raw("CONCAT(name,'',names,'',paternal_surname,'',maternal_surname,'',email,'',phone)"),'like',"%$search%")->paginate(15);

        return view('Users.index', compact('users','search'));
    }

    public function create()
    {
        //
    }

    public function roles($id)
    {
        $user = User::findOrfail($id);
        $roles = array();
        foreach ($user->getRoleNames() as $role) {
            array_push($roles, $role);
        }
        return view('Users.roles', compact('user','roles'));
    }

    public function update_roles(Request $request, $id)
    {
        $user = User::whereId($id)->first();

        $roles = json_decode($request->roles);

        foreach($roles as $role){
            if($role->current_role){
               $user->assignRole($role->name);
            }else{
                $user->removeRole($role->name);
            }
        }

        return response()->json(['response' => true], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'names'                 => 'required',
            'paternal_surname'      => 'required|alpha',
            'maternal_surname'      => 'required|alpha',
            'birthdate'             => 'required|date|date_format:"Y-m-d"',
            'nationality'           => 'required|alpha',
            'marital_status'        => 'required',
            'rfc'                   => 'required|alpha_num|min:12|max:13',
            'phone'                 => 'required|numeric|digits:10',
            'medical_insurance'     => 'required|numeric|digits:11',
            'emergency_contact'     => 'required',
            'contact_number'        => 'required|numeric|digits:10',
            'medication'            => 'nullable|alpha_num',
            'street'                => 'required',
            'gender'                => 'required',
            'neighborhood'          => 'required',
            'external_number'       => 'required|numeric',
            'internal_number'       => 'nullable|alpha_num',
            'zipcode'               => 'required|numeric|digits:5',
            'name'                  => 'required|alpha_num|unique:users,name',
            'email'                 => 'required|email|unique:users,email',
            'password'              => 'required|min:8',
            'password-confirmation' => 'required|same:password',
            'policy-privacy'        => 'accepted',
        ],[
            'names.required'                 => 'El nombre es requerido',
            'paternal_surname.required'      => 'El apellido paterno es requerido',
            'paternal_surname.alpha'         => 'El apellido paterno no puede contener caracteres especiales',
            'maternal_surname.required'      => 'El apellido materno es requerido',
            'maternal_surname.alpha'         => 'El apellido materno no puede contener caracteres especiales',
            'birthdate.required'             => 'La fecha de nacimiento es requerida',
            'birthdate.date'                 => 'La fecha de nacimiento no es válida',
            'birthdate.date_format'          => 'El formato de la fecha de nacimiento no es válido',
            'nationality.required'           => 'La nacionalidad es requerida',
            'nationality.alpha'              => 'La nacionalidad no puede contener caracteres especiales',
            'marital_status.required'        => 'El estado civil es requerido',
            'rfc.required'                   => 'El RFC es requerido',
            'rfc.alpha_num'                  => 'El RFC no puede contener caracteres especiales',
            'rfc.min'                        => 'El RFC debe tener mínimo 12 caracteres',
            'rfc.max'                        => 'El RFC debe contener máximo 13 caracteres',
            'gender.required'                => 'El campo sexo es requerido',
            'phone.required'                 => 'El teléfono es requerido',
            'phone.numeric'                  => 'El teléfono no es válido',
            'phone.digits'                   => 'El teléfono debe contener 10 números',
            'medical_insurance.required'     => 'El N° de seguro social es requerido',
            'medical_insurance.numeric'      => 'El N° de seguro social no es válido',
            'medical_insurance.digits'       => 'El N° de seguro social debe contener 11 caracteres',
            'emergency_contact.required'     => 'El contacto de emergencía es requerido',
            'contact_number.required'        => 'El teléfono de emergencía es requerido',
            'contact_number.numeric'         => 'El teléfono de emergencía no es válido',
            'contact_number.digits'          => 'El teléfono de emergencía debe ser númerico',
            'medication.alpha_num'           => 'La medicación no debe contener caracteres especiales',
            'street.required'                => 'El nombre de la calle es requerido',
            'external_number.required'       => 'El número exterior es requerido',
            'external_number.numeric'        => 'El número exterior no es válido',
            'internal_number.alpha_num'      => 'El número exterior no puede contener caracteres especiales',
            'zipcode.required'               => 'El código postal es requerido',
            'zipcode.numeric'                => 'El código postal no es válido',
            'zipcode.digits'                 => 'El código postal debe contener 5 caracteres',
            'name.required'                  => 'El nombre de usuario es requerido',
            'name.alpha_num'                 => 'El nombre de usuario no debe contener caracteres especiales',
            'name.unique'                    => 'El nombre de usuario ingresado ya está en uso',
            'email.required'                 => 'El correo electónico es requerido',
            'email.email'                    => 'El correo electónico ingresado no es válido',
            'email.unique'                   => 'El correo electónico ingresado ya está en uso',
            'password.required'              => 'La contraseña es requerida',
            'password.min'                   => 'La contraseña debe contener almenos 8 caracteres',
            'password-confirmation.required' => 'La confirmación de contraseña es requerida',
            'password-confirmation.same'     => 'La confirmación de la contraseña no coincide',
            'privacy-policy.accepted'        => 'Debe aceptar las políticas de privacidad para continuar'
        ]);

        $user = new User();
        $user->names             = $request->names;
        $user->name              = $request->name;
        $user->paternal_surname  = $request->paternal_surname;
        $user->maternal_surname  = $request->maternal_surname;
        $user->birthdate         = $request->birthdate;
        $user->nationality       = $request->nationality;
        $user->marital_status    = $request->marital_status;
        $user->street            = $request->street;
        $user->neighborhood      = $request->neighborhood;
        $user->external_number   = $request->external_number;
        $user->internal_number   = $request->internal_number;
        $user->zipcode           = $request->zipcode;
        $user->gender            = $request->gender;
        $user->rfc               = $request->rfc;
        $user->phone             = $request->phone;
        $user->email             = $request->email;
        $user->medical_insurance = $request->medical_insurance;
        $user->emergency_contact = $request->emergency_contact;
        $user->contact_number    = $request->contact_number;
        $user->medication        = $request->medication;
        $user->admission_date    = date('Y-m-d');
        $user->password          = bcrypt($request->password);

        $user->save();

        $user->assignRole('mesero');

        return redirect()->route('login')->withStatus('Datos registrados correctamente, se requiere aprovación del administrador para continuar.');
    }

    public function show($id)
    {
        $user = User::findOrfail($id);
        return view('Users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::findOrfail($id);
        return view('Users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'names'                 => 'required',
            'paternal_surname'      => 'required|alpha',
            'maternal_surname'      => 'required|alpha',
            'birthdate'             => 'required|date|date_format:"Y-m-d"',
            'nationality'           => 'required|alpha',
            'marital_status'        => 'required',
            'rfc'                   => ['required','alpha_num','min:12','max:13', Rule::unique('users')->ignore($id)],
            'phone'                 => 'required|numeric|digits:10',
            'medical_insurance'     => 'required|numeric|digits:11',
            'emergency_contact'     => 'required',
            'contact_number'        => 'required|numeric|digits:10',
            'medication'            => 'nullable|alpha_num',
            'street'                => 'required',
            'gender'                => 'required',
            'neighborhood'          => 'required',
            'external_number'       => 'required|numeric',
            'internal_number'       => 'nullable|alpha_num',
            'zipcode'               => 'required|numeric|digits:5',
            'name'                  => ['required','alpha_num',Rule::unique('users')->ignore($id)],
            'email'                 => ['required','email', Rule::unique('users')->ignore($id)],
            'user-image'            => 'nullable|image',
        ],[
            'names.required'                 => 'El nombre es requerido',
            'paternal_surname.required'      => 'El apellido paterno es requerido',
            'paternal_surname.alpha'         => 'El apellido paterno no puede contener caracteres especiales',
            'maternal_surname.required'      => 'El apellido materno es requerido',
            'maternal_surname.alpha'         => 'El apellido materno no puede contener caracteres especiales',
            'birthdate.required'             => 'La fecha de nacimiento es requerida',
            'birthdate.date'                 => 'La fecha de nacimiento no es válida',
            'birthdate.date_format'          => 'El formato de la fecha de nacimiento no es válido',
            'nationality.required'           => 'La nacionalidad es requerida',
            'nationality.alpha'              => 'La nacionalidad no puede contener caracteres especiales',
            'marital_status.required'        => 'El estado civil es requerido',
            'rfc.required'                   => 'El RFC es requerido',
            'rfc.alpha_num'                  => 'El RFC no puede contener caracteres especiales',
            'rfc.min'                        => 'El RFC debe tener mínimo 12 caracteres',
            'rfc.max'                        => 'El RFC debe contener máximo 13 caracteres',
            'rfc.unique'                     => 'El RFC ingresado ya está en uso',
            'gender.required'                => 'El campo sexo es requerido',
            'phone.required'                 => 'El teléfono es requerido',
            'phone.numeric'                  => 'El teléfono no es válido',
            'phone.digits'                   => 'El teléfono debe contener 10 números',
            'medical_insurance.required'     => 'El N° de seguro social es requerido',
            'medical_insurance.numeric'      => 'El N° de seguro social no es válido',
            'medical_insurance.digits'       => 'El N° de seguro social debe contener 11 caracteres',
            'emergency_contact.required'     => 'El contacto de emergencía es requerido',
            'contact_number.required'        => 'El teléfono de emergencía es requerido',
            'contact_number.numeric'         => 'El teléfono de emergencía no es válido',
            'contact_number.digits'          => 'El teléfono de emergencía debe ser númerico',
            'medication.alpha_num'           => 'La medicación no debe contener caracteres especiales',
            'street.required'                => 'El nombre de la calle es requerido',
            'external_number.required'       => 'El número exterior es requerido',
            'external_number.numeric'        => 'El número exterior no es válido',
            'internal_number.alpha_num'      => 'El número exterior no puede contener caracteres especiales',
            'zipcode.required'               => 'El código postal es requerido',
            'zipcode.numeric'                => 'El código postal no es válido',
            'zipcode.digits'                 => 'El código postal debe contener 5 caracteres',
            'name.required'                  => 'El nombre de usuario es requerido',
            'name.alpha_num'                 => 'El nombre de usuario no debe contener caracteres especiales',
            'name.unique'                    => 'El nombre de usuario ingresado ya está en uso',
            'email.required'                 => 'El correo electónico es requerido',
            'email.email'                    => 'El correo electónico ingresado no es válido',
            'email.unique'                   => 'El correo electónico ingresado ya está en uso',
            'user-image.image'               => 'La imagen ingresada no es válida'
        ]);

        $user = User::findOrfail($id);

        if($request->file('user-image')){
            if($user->image != null){
                \File::delete('images/users_images/'.$user->image);
            }
            $file = $request->file('user-image');
            $name = uniqid().'.'.$file->getClientOriginalExtension();
            $destination_path = public_path('images/users_images');
            $file->move($destination_path,$name);

            $user->image = $name;
        }

        $user->names             = $request->names;
        $user->name              = $request->name;
        $user->paternal_surname  = $request->paternal_surname;
        $user->maternal_surname  = $request->maternal_surname;
        $user->birthdate         = $request->birthdate;
        $user->nationality       = $request->nationality;
        $user->marital_status    = $request->marital_status;
        $user->street            = $request->street;
        $user->neighborhood      = $request->neighborhood;
        $user->external_number   = $request->external_number;
        $user->internal_number   = $request->internal_number;
        $user->zipcode           = $request->zipcode;
        $user->gender            = $request->gender;
        $user->rfc               = $request->rfc;
        $user->phone             = $request->phone;
        $user->email             = $request->email;
        $user->medical_insurance = $request->medical_insurance;
        $user->emergency_contact = $request->emergency_contact;
        $user->contact_number    = $request->contact_number;
        $user->medication        = $request->medication;

        $user->update();

        return redirect()->route('users.index')->withStatus('Usuario actualizado correctamente');


    }

    public function authorizeAccount(Request $request)
    {
        $user = User::findOrfail($request->id);
        $user->authorized = true;
        $user->save();
        // return (new AuthorizedAccount($user))->render();
        Mail::to($user->email)->send(new AuthorizedAccount($user));

        return redirect()->route('users.index')->withStatus('Usuario autorizado correctamente');
    }

    public function disable(Request $request)
    {
        $user = User::findOrfail($request->id);
        if($user->hasRole('super-admin'))
            return redirect()->route('users.index')->withErrors('El usuario no puede ser desactivado');

        $user->status = false;
        $user->update();

        return redirect()->route('users.index')->withStatus('Usuario desactivado correctamente');
    }

    public function enable(Request $request)
    {
        $user = User::findOrfail($request->id);
        $user->status = true;
        $user->save();

        return redirect()->route('users.index')->withStatus('Usuario activado correctamente');
    }

    public function destroy(Request $request)
    {
        $user = User::findOrfail($request->id);
        if($user->hasRole('super-admin'))
            return redirect()->route('users.index')->withErrors('No puede eliminar este usuario');

        $user->delete();
        return redirect()->route('users.index')->withStatus('Usuario eliminado correctamente');
    }
}

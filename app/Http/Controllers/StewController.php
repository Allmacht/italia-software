<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stew;

class StewController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $stews = Stew::where('name','like',"%$search%")->get();
        return view('Stews.index',compact('search','stews'));
    }

    public function store(Request $request)
    {

        $stew = new Stew();
        $stew->name = $request->name;
        $stew->save();

        return redirect()->route('stews.index')->withStatus('Guisado registrado correctamente');
    }

    public function disable($id)
    {
        $stew = Stew::findOrfail($id);
        $stew->status = false;
        $stew->update();

        return redirect()->route('stews.index')->withStatus('Guisado desactivado correctamente');
    }

    public function enable($id){
        $stew = Stew::findOrfail($id);
        $stew->status = true;
        $stew->update();

        return redirect()->route('stews.index')->withStatus('Guisado activado correctamente');
    }
}
